from natasha import (
    Segmenter,
    MorphVocab,

    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    NewsNERTagger,

    PER,
    NamesExtractor,
    DatesExtractor,

    Doc
)
import requests


# получение персон и достопремечательностей
response_1 = requests.get('https://vue-with-http-499c5-default-rtdb.firebaseio.com/places.json').json()

response_persons = requests.get('https://vue-with-http-499c5-default-rtdb.firebaseio.com/persons.json').json()

res_places = response_1["-NLi5Spnik1j1qpu1lC9"] + response_persons["-NLgVzOyOLzul6lZ4_ta"]

# массив где будут храниться окончания
places = []

# удаление окончаний со слов
for elem in res_places:
    list = [_ for _ in elem.replace(".", "").replace("-", " ").replace("\"", "").split(" ") if len(_) > 2]

    for i in range(len(list)):
        if "я" == list[i][-1:] or "го" == list[-2:]:
            list[i] = list[i][:-2]
        else:
            list[i] = list[i][:-1]

    places.append(list)


# обьект которой будет хранить результат, то есть предложения
result = {}


# первоначальная насторойка обьекта
for item in res_places:
    result[item] = {
        "2023-01": [],
        "2022-12": [],
        "2022-11": [],
        "2022-10": [],
        "2022-09": [],
        "2022-08": [],
        "2022-07": []
    }


try:
    # настройка natash'ы
    segmenter = Segmenter()
    morph_vocab = MorphVocab()

    emb = NewsEmbedding()
    morph_tagger = NewsMorphTagger(emb)
    syntax_parser = NewsSyntaxParser(emb)
    ner_tagger = NewsNERTagger(emb)

    names_extractor = NamesExtractor(morph_vocab)
    dates_extractor = DatesExtractor(morph_vocab)

    # --------------------------------------------------------

    # получение новостей с бд
    response = requests.get(
        'https://test-34479-default-rtdb.firebaseio.com/news.json').json()

    count_length = 0

    # for прохода по элементам
    for item in response:
        # получение текста статьи
        text = response[item]['text']
        doc = Doc(text)
        doc.segment(segmenter)
        doc.tag_morph(morph_tagger)
        doc.parse_syntax(syntax_parser)
        doc.tag_ner(ner_tagger)

        check = False

        # for прохода по предложениям
        for sent in doc.sents:
            index_places = 0
            # for прохода по массиму имен; ['FirstName', 'SecondName', 'ThirdName']
            for items_places in places:
                count = 0
                # for прохода по массиву [имен, фамилии, очества]
                for word_place in items_places:
                    # проверка вхождения слова в предложение
                    if word_place.lower() in sent.text.lower():
                        # счет совпадений
                        count+=1
                    if count > 1:
                        # Если количество совпадений больше одного, то создаем запись в бд и сохраняем в result 
                        result[res_places[index_places]][f"{response[item]['year']}-{response[item]['month']}"].append({
                            "sentence": sent.text,
                            "id": str(item),
                            "url": response[item]['href']
                        })
                        print('\n--------' + res_places[index_places])
                        print({
                            "sentence": sent.text,
                            "id": str(item),
                            "url": response[item]['href']
                        })
                        # счет количества найденных записей/предложений
                        count_length+=1
                        check = True
                        break
                if check == True:
                    break

                index_places+=1

            if check == True:
                break

    # вывод количества найденых предложений
    print("\n" + str(count_length) + "\n")
    # запись в бд   
    requests.post(url="https://test-34479-default-rtdb.firebaseio.com/sentenses_parsion_init_site.json", json=result)

    

 
except KeyboardInterrupt:
    print()