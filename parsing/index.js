import axios from 'axios'
import { load } from 'cheerio'

async function init() {
    for (let i = 1; i < 10; i++) {
        const { data } = await axios('https://gorvesti.ru/feed/' + i) // https://gorvesti.ru/feed/1
        const $ = load(data)

        $('.itm-title>a').each(async function (index, elem) {
            // получаем ссылку новости
            let href = "https://gorvesti.ru" + $(elem).attr("href")


            const response = await axios(href)
            const $href = load(response.data)
            // header
            const header_news = $href('.article-title-block>h1.title-block').text()
            // date
            const date = $href('.article-title-block>meta').attr('content').split('T')[0]
            // text news
            let text_news = ""
            $href('article.item.block>p').each((i, elem) => {
                text_news += $(elem).text() + " "
            })

            const result = {
                title: header_news,
                date: date,
                text: text_news,
                href: href,
                year: date.split('-')[0],
                month: date.split('-')[1]
            }

            await axios.post("https://test-34479-default-rtdb.firebaseio.com/news_site.json", result)
        })
        console.log(i);
    }
}

init()