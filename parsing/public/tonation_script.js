window.onload = async () => {
    await go()
}

async function go() {
    const res = await fetch("https://test-34479-default-rtdb.firebaseio.com/sentenses_rate_in_percent/-NMA4qVYpWi7ONmn-sck.json")
    const data = await res.json()

    for (let item in data) {
        document.querySelector('tbody').innerHTML += `
        <tr>
            <th scope="row">${item}</th>
            <td>${data[item]["2022-07"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2022-08"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2022-09"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2022-10"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2022-11"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2022-12"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2023-01"] ?? "Нет упоминаний"}</td>
        </tr>
        `
    }
}