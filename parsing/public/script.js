window.onload = async () => {
    await go()
}

async function go() {
    console.log('1');
    const res = await fetch("https://test-34479-default-rtdb.firebaseio.com/news_site.json")
    const data = await res.json()

    for (let item in data) {
        document.querySelector('tbody').innerHTML += `
        <tr>
            <th scope="row">${item}</th>
            <td>${data[item].date}</td>
            <td>${data[item].title}</td>
            <td class="overflow-hidden" >
                <div style="white-space: nowrap; width: 300px; display: inline-block">${data[item].text}</div>
            </td>
            <td>${data[item].href}</td>
        </tr>
        `
    }
}